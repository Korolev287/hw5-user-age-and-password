// Экранирование нужно - чтобы использовать специальный символ как обычный.

function createNewUser() {
    const userName = prompt("Введите ваше имя");
    const userLastName = prompt("Введите вашу фамилию");
    const birthday = prompt("Введите вашу дату рождения");
    return {
        firstName: userName,
        lastName: userLastName,
        getLogin: function () {
            console.log(userName.charAt(0).toLowerCase() + userLastName.toLowerCase());
        },
        getAge: function () {
            const day = parseInt(birthday.substring(0, 2));
            const month = parseInt(birthday.substring(3, 5));
            const year = parseInt(birthday.substring(6, 10));

            const currentDate = new Date();
            const birthDate = new Date(year, month - 1, day);
            let age = currentDate.getFullYear() - birthDate.getFullYear();
            const monthCheck = currentDate.getMonth() - birthDate.getMonth();
            if (monthCheck < 0 || (monthCheck === 0 && currentDate.getDate() < birthDate.getDate())) {
                age--;
            }
            return console.log(age);
        },
        getPassword: function () {
            console.log(userName.charAt(0).toUpperCase() + userLastName.toLowerCase() + birthday.substring(6, 10));
        },
    };
}


const newUser = createNewUser();
console.log(newUser);
newUser.getAge();
newUser.getPassword();

